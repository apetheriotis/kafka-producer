package gr.tuc.apetheriotis.kafkaproducer

import java.util.Properties

import kafka.producer.{KeyedMessage, Producer, ProducerConfig}

import scala.util.Random


object Producer {


  def main(args: Array[String]): Unit = {


    if (args.length < 3) {
      System.err.println("Usage: KafkaWordCount <brokers> <topic> <sleep>")
      System.exit(1)
    }

    val Array(brokers, topic, sleep) = args


    // Zookeper connection properties
    val props = new Properties()
    props.put("metadata.broker.list", brokers)
    props.put("serializer.class", "kafka.serializer.StringEncoder")
    props.put("producer.type", "async")
    props.put("queue.enqueue.timeout.ms", "-1")
    props.put("batch.num.messages", "10")

    // Producer configuration
    val config = new ProducerConfig(props)
    val producer = new Producer[String, String](config)


    var startTime = 0L
    var indexNo = 0
    var no = 0
    while (true) {
      val lola = new Random().nextInt(10000)
      val line = "3274_____58779_____1_____I like the recursive approach, the risk of a stack overflow is minimum at worst._____2008-09-12T12:58:05.223_____" + lola
      producer.send(List(new KeyedMessage[String, String](topic, no + "", line)).toSeq: _*)
      indexNo = indexNo + 1
      if (System.currentTimeMillis() - startTime > 1000) {
        startTime = System.currentTimeMillis()
        println(indexNo + " web logs messages/s")
        indexNo = 0
      }
      no = no + 1
      Thread.sleep(sleep.toInt)

    }


    //    var startTime = 0L
    //    var indexNo = 0
    //    var no = 0
    //    for (line <- Source.fromFile(PATH_TO_FILE).getLines()) {
    //
    //      try {
    //        val userId = line.split("_____")(5).toLong
    //        println(line)
    //
    //        producer.send(List(new KeyedMessage[String, String](TOPIC, no + "", line)).toSeq: _*)
    //        indexNo = indexNo + 1
    //        no = no + 1
    //        Thread.sleep(0)
    //        if (System.currentTimeMillis() - startTime > 1000) {
    //         startTime = System.currentTimeMillis()
    //          println(indexNo + " web logs messages/s")
    //
    //          indexNo = 0
    //
    //        }
    //
    //
    //      } catch {
    //        case e: Exception =>
    //
    //      }
    //
    //    }
  }


}
